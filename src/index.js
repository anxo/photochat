const cors = require('cors')
const express = require('express')
const fs = require('fs')
const http = require('http')
const upload = require('multer')()
const ws = require('ws')

const port = process.env.PORT || 5000
const prefix = process.env.DOMAIN || 'http://localhost:5000'

const app = express()
const server = http.createServer(app)

// Chat data
const images = []
const messages = []
const maxImages = 24
const maxMessages = 50

// Socket clients
const clients = []
const broadcast = (message) => {
  const t = JSON.stringify(message)
  clients.forEach(c => c.send(t))
}

// Server routes
app.use(cors())
app.use(express.json())
app.get('/', (req, res) => {
  res.contentType('html')
  res.send('<pre>' + fs.readFileSync('./README.md') + '</pre>')
})
app.get('/messages', (req, res) => {
  res.send(messages);
})
app.post('/message', (req, res) => {
  if (!req.body || !req.body.name) {
    res.status(400).send({ error: 'Missing required field: name'})
  } else if (!req.body.message) {
    res.status(400).send({ error: 'Missing required field: message'})
  } else {
    const message = {
      name: req.body.name,
      message: req.body.message,
      date: Date.now()
    }
    messages.push(message)
    if (messages.length > maxMessages) messages.shift()
    res.send(message)
    broadcast({ messages: [message] })
  }
})
app.get('/images', (req, res) => {
  res.send(images.map(({ data, ...other }) => other));
})
app.post('/image', upload.single('image'), (req, res) => {
  if (!req.body || !req.body.name) {
    res.status(400).send({ error: 'Missing required field: name'})
  } else if (!req.file) {
    res.status(400).send({ error: 'Missing required field: image'})
  } else {
    const id = Date.now()
    const image = {
      date: id,
      name: req.file.originalname,
      url: prefix + '/image/' + id,
    }
    images.push(image)
    if (images.length > maxImages) images.shift()
    res.send(image)
    broadcast({ images: [image] })
    image.data = req.file.buffer
  }
})
app.get('/image/:id', (req, res) => {
  id = parseInt(req.params.id)
  const img = images.find(i => i.date === id)
  if (!img) {
    res.status(404).send('Not found')
  } else {
    res.send(img.data)
  }
})

// WS handling
var wss = new ws.Server({ server, path: '/ws' })
wss.on('connection', socket => {
  clients.push(socket)
  const i = images.map(({ date, name, url }) => ({ date, name, url }))
  socket.send(JSON.stringify({ images: i, messages }))
  socket.on('close', () => {
    const n = clients.indexOf(socket)
    if (n >= 0) clients.splice(n, 1)
  })
})

server.listen(port)
console.log('*** Server listening at: http://localhost:' + port)
