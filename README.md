# chat-api

A demo API implementing a tiny chat server with image upload, and a WS notification endpoint

## Endpoints

The following endpoints are implemented:

- `GET /`: shows this help
- `GET /messages`: lists current messages
- `POST /message`: sends a message (required fields: `name`, `message`)
- `GET /images`: lists current images
- `POST /image`: sends an image (required fields: `name`, `image`)
- `GET /image/:id`: retrieves an image
- `WS /ws`: the notification websocket
